%% Autofocus
%This script is a basic example of an autofocus algorithm 
%           
% J. Gladines
% Feb./2022

%% default settings
N = 150;                     % number of images for approach 1
roi = [450, 590, 100, 100];   % region of interest if needed. format: [x0, y0, W, H]
useROI = false;              % Set to false if you don't want to use ROI
fmo = 'TENV';               %Focus measure operator to use (choose from focusmeasure.m and paper by S.Pertuz
imin = 0;                       % start current of the lens
imax = 292;                     % stop current of the lens
current = linspace(imin, imax, N); %make an array with linearly distributed current values for the lens.
offset = -13;                   %manual offset to the autofocus maximum
lensCOM = 'COM5';           %Com port of the tunable lens (can be found in the device manager on windows)

%% Open camera and lens

%put here the code to connect to the camera
%cam = PleoraCamera();
%cam.Open();


lens = Optotune(lensCOM);    %Connect to and open the tunable lens
lens.Open();
lens.setCurrent(0);


%% Autofocus (approach 1)
% scan over the whole range and determine the point of maximum focus
focus = zeros(1, N);

for i = 1:N
    lens.setCurrent(current(i))
    
    %put here the code to take an image from the camera
    %I = cam.GetFrame();

    if useROI
        I = imcrop(I, roi);             %crop the image to the region of interest
    end
    focus(i) = focusmeasure(I, fmo, 0); %wsize is  put to 0 for autofocus (it uses the full window)
end

plot(current, focus);                   %plot the focus function in an figure


%Gaussian fitting of focus curve to find precise maximum
[y2, x2] = max(focus);                  %finds maximum value in focus curve (but is discrete value)

step = round(length(focus)/25);
x1 = x2 - step;
x3 = x2 + step;
y1 = focus(x1);
y3 = focus(x3);

c = ( (y1-y2).*(x2-x3)-(y2-y3).*(x1-x2) )./...
    ( (x1.^2-x2.^2).*(x2-x3)-(x2.^2-x3.^2).*(x1-x2) );
b = ( (y2-y3)-c.*(x2-x3).*(x2+x3) )./(x2-x3);
a = y1 - b.*x1 - c.*x1.^2;

s = sqrt(-1./(2*c));
m = b.*s.^2;                    % m = maximum focus point index
c = ((imax-imin)/N)*m;

% phase correlation for finding maximum (optional approach)
fft_fm = fft(focus);
out_fft = -(angle(fft_fm(2)./exp(-sqrt(-1).*pi)));
c2 = ((imax-imin)/N)*((out_fft+pi) .* (N/(2*pi)));

% set lens to maximum focus 
lens.setCurrent(c+offset);
cam.Close();


%lens.Close()       %to close the connection to the lens, this will reset the current to 0;

%% Approach 2 (iteratively find maximum focus)
%TBC
