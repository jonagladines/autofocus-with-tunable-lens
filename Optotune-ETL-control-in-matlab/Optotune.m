classdef Optotune < handle
    
    % Optotune class is designed to control electro tunable lens from optotune.com
    % The script is written by R. Spesyvtsev from the Optical manipulation group (http://www.st-andrews.ac.uk/~photon/manipulation/)
    % Last modification by Zhengyi Yang
    %
    % constructor Optotune(com port), example lens = Optotune('COM9');
    % connect: lens = lens.Open();
    % set current to 100 mA:  lens = lens.setCurrent(100);
    % lens can be set to different modes: 
    % Current/Focal Power/Analog/Sinusoidal/Rectangular/Triangular
    % mode LowerCurrent/UpperCurrent/Frequency includes the parameters.
    
    % checkError will check the response if there is error and what error
    
    properties
        etl_port;
        port;
        status;
        response;
        
        temperature = 0;
        current = 0; %% in mAmpers
        max_current = 0; % in mAmpers
        min_current = 0; % in mAmpers
        
        modeLowerCurrent = 0;
        modeUpperCurrent = 0;
        modeFrequency = 1;
        
        analogInput = 0;
        max_bin = 0;
        time_pause = 0.3;
        time_laps = 0.01;
        last_time_laps;
        
        error = false;
    end
    
%default methods   
    methods
        %constructor
        function lens=Optotune(port)
            if (nargin<1)
                lens.port='COM3';
            else
                lens.port = port;
            end
        end
  
        %Setting up initial parameters for the com port
        function Open(lens)

            lens.etl_port = serial(lens.port);
            lens.etl_port.Baudrate=115200;
            lens.etl_port.StopBits=1;
            lens.etl_port.Parity='none';
            
            fopen(lens.etl_port);
            lens.status = lens.etl_port.Status;   %%% checking if initialization was completed the status should be "open"
            
            %Initialize communication
            fprintf(lens.etl_port, 'Start'); %%% initiating the communication
            lens.last_time_laps = checkStatus(lens);
            %lens.etl_port.BytesAvailable;  %%% checking number of bytes in the response
            fscanf(lens.etl_port);  %%% reading out the response which should read "Ready"
            if lens.etl_port.BytesAvailable
                fread(lens.etl_port,lens.etl_port.BytesAvailable);
            end
            
            %get initial information about UpperCurrentLimte and MaxBin;
            lens.getMaxBin();
            lens.getUpperLimitA();
            %Get initial parameters
            lens.getModeFrequency();
            lens.getModeLowerCurrent();
            lens.getModeUpperCurrent();
            lens.getTemperature();

        end

%% getters en setters
        function temperature = getTemperature(lens)
            command = append_crc('TA'-0);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            %pause(lens.time_pause);
            %lens.etl_port.BytesAvailable;  %%% checking number of bytes in the response
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            x = lens.response(4)*(hex2dec('ff')+1) + lens.response(5);
            temperature = x*0.0625;
            lens.temperature = temperature;
        end
        
        %Get current in mA 
        function current = getCurrent(lens)
            command = append_crc(['Ar'-0 0 0]);
            fwrite(lens.etl_port, command);
            %pause(lens.time_pause);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            current = lens.response(2)*(hex2dec('ff')+1) + lens.response(3);
            current = current * lens.max_current / (lens.max_bin+1);
            lens.current = current;
        end
        
        %Set current in mA via ci variable
        function setCurrent(lens, ci) 
            set_i = (floor(ci*(lens.max_bin+1) / lens.max_current));
            LB = mod(set_i,256); %% low byte
            HB = (set_i-LB)/256; %% high byte
            command = append_crc(['Aw'-0 HB LB]);
            fwrite(lens.etl_port, command);
            %pause(lens.time_pause);
        end
        
        %setting temperature limits for operation in focal power mode
        function setTemperatureLimits(lens)
            % The function has not been finished
            temp_high = 80/0.0625;  %% Upper limit 80 degrees (see Optotune manual for available limits)
            temp_low = 0/0.0625;     %% Lower limit 0 degrees
            LBH = mod(temp_high,256); %% low byte
            HBH = (temp_high-LBH)/256; %% high byte
            LBL = mod(temp_low,256); %% low byte
            HBL = (temp_low-LBL)/256; %% high byte
            command = append_crc(['PrTA'-0 HBH LBH HBL LBL]);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
        end
        
        function max_bin = getMaxBin(lens)
            command = append_crc(['CrUA'-0 0 0]);
            fwrite(lens.etl_port, command);
            %pause(lens.time_pause);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            max_bin = lens.response(4)*(hex2dec('ff')+1) + lens.response(5)+1;  %% hardware current limit usually 4095;
            lens.max_bin = max_bin;
        end
       
        %Get upper current limit
        function max_current = getUpperLimitA(lens)
            command = append_crc(['CrMA'-0 0 0]);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            %pause(lens.time_pause);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            max_current = lens.response(4)*(hex2dec('ff')+1) + lens.response(5);  %% software current limit usually 292.84 mA;
            max_current = max_current / 100; %% reads current in mili ampers
            lens.max_current = max_current;
        end
        
%% set the lens to different mode
        
        %set the mode of the lens, choose from ['sinuoidal', 'rectangular', 'current', 'triangular', 'focal', 'analog']
        function setMode(lens, mode)
            switch(mode)
                case 'sinusoidal'        
                    command = 'MwSA';
                case 'rectangular'     
                    command = 'MwQA';
                case 'current'        
                    command = 'MwDA';
                case 'triangular'     
                    command = 'MwTA';
                case 'focal'        
                    command = 'MwCA';
                case 'analog'       
                    command = 'MwAA';
                otherwise
                    warning(["Mode" mode "does not exist"]);
                    return
            end
            
            command = append_crc(command-0);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            mode_response = char(lens.response(1:3))';
            
            switch(mode_response)
                case 'MSA'
                    fprintf('Lens set to Sinusoidal Mode succesfully\n');
                case 'MQA'
                    fprintf('Lens set to Rectangular Mode succesfully\n');
                case 'MDA'
                    fprintf('Lens set to Current Mode succesfully\n');
                case 'MTA'
                    fprintf('Lens set to Triangular Mode succesfully\n');
                case 'MCA'
                    fprintf('Lens set to Focal Power Mode succesfully\n');
                case 'MAA'
                    fprintf('Lens set to Analog Mode succesfully\n');
                otherwise
                    checkError(lens);  
            end
        end
        
        %get the current mode the lens is running at
        function getMode(lens)
            command = append_crc('MMA'-0);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            checkError(lens);
            switch lens.response(4)
                case 1
                    fprintf('Lens is driven in Current mode');
                case 2
                    fprintf('Lens is driven in Sinusoidal Singal mode');
                case 3
                    fprintf('Lens is driven in Triangular mode');
                case 4
                    fprintf('Lens is driven in Retangular mode');
                case 5
                    fprintf('Lens is driven in FocalPower mode');
                case 6
                    fprintf('Lens is driven in Analog mode');
                case 7
                    fprintf('Lens is driven in Position Controlled Mode');
            end
        end
        
%% Set parameters for the mode controlling
        
        % set signal generator upper current limit
        function setModeUpperCurrent(lens,ci)
            if ci > lens.max_current || ci < 0
                fprintf('The current should be between 0 and %.2f mA', lens.max_current);
                ci = lens.max_current;
            else
                if ci < lens.modeLowerCurrent
                    lens = lens.setModeLowerCurrent(ci);
                end
            end
            set_i = (floor(ci*(lens.max_bin+1) / lens.max_current));
            LB = mod(set_i,256); %% low byte
            HB = (set_i-LB)/256; %% high byte
            command = append_crc(['PwUA'-0 HB LB 0 0]);
            fwrite(lens.etl_port, command);
            checkError(lens);
            if lens.error == true
                fprintf('Error occurred when setting modeUpperCurrent');
            end
            

                
            getModeUpperCurrent(lens);
        end
        
        % get signal generator upper current limit
        function getModeUpperCurrent(lens)
            command = append_crc(['PrUA'-0 0 0 0 0]);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
             if strcmp(cellstr(char(lens.response(1:4))'), 'MPAU')
                lens.modeUpperCurrent = (lens.response(5)*(hex2dec('ff')+1) + lens.response(6))*lens.max_current/lens.max_bin;
            else
                checkError(lens);
                if lens.error == true
                    fprintf('Error occurred when getting modeUpperCurrent');
                end
            end
        end
        
        % set signal generator lower current limit
        function setModeLowerCurrent(lens,ci)
            if ci > lens.max_current || ci < 0
                fprintf('The current should be between 0 and %.2f mA', lens.max_current);
                ci = 0;
            else
                if ci > lens.modeUpperCurrent
                    lens = lens.setModeUpperCurrent(ci);
                end
            end
            set_i = (floor(ci*(lens.max_bin+1) / lens.max_current));
            LB = mod(set_i,256); %% low byte
            HB = (set_i-LB)/256; %% high byte
            command = append_crc(['PwLA'-0 HB LB 0 0]);
            fwrite(lens.etl_port, command);
            checkError(lens);
            if lens.error == true
                fprintf('Error occurred when setting modeLowerCurrent');
            end
                       
            getModeLowerCurrent(lens);
        end
        
        % get signal generator lower current limit
        function getModeLowerCurrent(lens)
            command = append_crc(['PrLA'-0 0 0 0 0]);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            if strcmp(cellstr(char(lens.response(1:4))'), 'MPAL')
                lens.modeLowerCurrent = (lens.response(5)*(hex2dec('ff')+1) + lens.response(6))*lens.max_current/lens.max_bin;  %% software current limit usually 292.84 mA;
            else
                checkError(lens);
                if lens.error == true
                    fprintf('Error occurred when getting modeLowerCurrent');
                end
            end
        end
        
        % set signal generator lower current limit
        function setModeFrequency(lens,ci)
            if ci < 0.1
                fprintf('The minimum frequency is 0.1 Hz!');
                ci = 0.1;
            end
            set_i = ci*1000;
            B4 = mod(set_i,256); %% 4th byte
            B3 = mod((set_i-B4)/256,256); %% third byte
            B2 = mod((set_i-B3*256-B4),256); %% second byte
            B1 = mod((set_i-B2*2^16-B3*256-B4),256); %% first byte
            command = append_crc(['PwFA'-0 B1 B2 B3 B4]);
            fwrite(lens.etl_port, command);
            checkError(lens);
            if lens.error == true
                fprintf('Error occurred when setting modeFrequency');
            end
            
            getModeFrequency(lens);
        end
        
        % get signal generator lower current limit
        function getModeFrequency(lens)
            command = append_crc(['PrFA'-0 0 0 0 0]);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            if strcmp(cellstr(char(lens.response(1:4))'), 'MPAF')
                lens.modeFrequency = (lens.response(5) * 2^24 + lens.response(6) * 2^16 + lens.response(7) * 2^8 + lens.response(8))/1000;  %% software current limit usually 292.84 mA;
            else
                checkError(lens);
                if lens.error == true
                    fprintf('Error occurred when getting modeFrequency');
                end
            end
        end
         
        function getAnalogInput(lens)
            command = append_crc('GAA'-0);
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            lens.analogInput = lens.response(4)*(hex2dec('ff')+1) + lens.response(5);
        end
        
        function testError(lens)
            command = 'St4rt';
            fwrite(lens.etl_port, command);
            lens.last_time_laps = checkStatus(lens);
            lens.response = fread(lens.etl_port,lens.etl_port.BytesAvailable);
            checkError(lens);
        end
        
%%  Closing the port when finished using it
        function Close(lens)
            if(strcmp(lens.status, 'open'))
                disp("Closing Optotune");
                lens.setMode('current');
                lens.setCurrent(0);
                fclose(lens.etl_port);
                delete(lens.etl_port);
                clear lens.etl_port

                lens.status = 'closed';
                lens.response = 'Shut down';
            else
                disp('Lens already closed');
            end
        end
        
        function delete(lens)
            lens.Close;
            disp('Lens deleted');
        end
        
        function tElapsed = checkStatus(lens)
            bts = lens.etl_port.BytesAvailable;  %%% checking number of bytes in the response
            tStart = tic;
            tElapsed = 0;
            while (bts ==0) || (tElapsed >5)
                bts = lens.etl_port.BytesAvailable;  %%% checking number of bytes in the response
                pause(lens.time_laps);
                tElapsed = toc(tStart);
            end
        end
        
        %Check if there is error message and identify what it is, display.
        function checkError(lens)
            lens.error = false;
            if char(lens.response(1)) == 'E'
                switch lens.response(2)
                    case 1
                        fprintf('CRC failed.');
                    case 2
                        fprintf('Command not available in firmware type.');
                    case 3
                        fprintf('Command not regongnized.');
                    case 4
                        fprintf('Lens is not compatible with firmware.');
                        
                end
                lens.error = true;
            end
        end
    end
end
